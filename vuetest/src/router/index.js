import Vue from 'vue'
import VueRouter from 'vue-router'
import Formulario from '../views/Formulario.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Formulario',
    component: Formulario,
    children: [
      {
        path: '/:formulario',
        component: () => import('../views/Formulario.vue'),
      }
    ]
  },
  {
    path: '/user',
    name: 'User',
    component: Formulario,
    children: [
      {
        path: '/:user',
        component: () => import('../views/Formulario.vue'),
      }
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
