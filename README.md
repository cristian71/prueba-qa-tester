Reglas para realizar QA:

**Introducción**

Existen 2 repositorios:

 1. vuetest (que representa el front en una app cualquiera ficticia)
 2. expresstest (que representa una API cualquiera ficticia y ocupa el puerto 5001)

**Actividad**

- Crear una rama llamada con la inicial de tu nombre y tu apellido completo, a partir de main para realizar las pruebas.

- Se solicita crear los siguientes test automatizados o manuales.

    1. Comprobar que no se pueda acceder al modulo de user sin estar logueado.
    2. Comprobar que la redirección con las credenciales válidas va al componente user (usar credenciales _admin_|_admin_)
    3. Validar que la respuesta de la API al enviar por metodo POST retorne Ok o error según si las credenciales son válidas o inválidas respectivamente (endpoint POST /)
    4. Validar que la información que retorna el endpoint (GET /:id donde el id es numérico) corresponda a un usuario válido.
